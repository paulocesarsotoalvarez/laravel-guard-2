FROM php:7.1.22-fpm

RUN docker-php-ext-install pdo_mysql
RUN apt-get update && apt-get install -y \
        libpq-dev \
        libpng-dev \
        zlib1g-dev \
        libmcrypt-dev \
        curl \
        openssl \
        zip \
        unzip \
        git \
        vim \
        libxml2-dev \
        gnupg \
        gnupg2 \
        gnupg1 \
        libpng-dev \
    && docker-php-ext-install -j$(nproc) zip \
    && docker-php-ext-install -j$(nproc) mcrypt \
    && docker-php-ext-install -j$(nproc) mbstring \
    && docker-php-ext-install -j$(nproc) gd \
    && docker-php-ext-install -j$(nproc) pdo \
    && docker-php-ext-install -j$(nproc) pdo_mysql \
    && docker-php-ext-install -j$(nproc) xml \
    && docker-php-ext-install -j$(nproc) bcmath

RUN curl -sS https://getcomposer.org/installer | \
php -- --install-dir=/usr/bin/ --filename=composer

WORKDIR /backend
COPY . .
RUN /usr/bin/composer install --prefer-dist --no-ansi --no-interaction --no-progress --no-scripts

RUN php artisan cache:clear

RUN usermod -a -G www-data root
RUN chgrp -R www-data storage

RUN chown -R www-data:www-data .
RUN chown -R www-data:www-data ./storage
RUN chmod -R 0777 ./storage
RUN chown -R www-data:www-data ./bootstrap/cache
RUN chmod -R 0777 ./bootstrap/cache
